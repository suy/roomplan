import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0

Rectangle {
    id: main
    property alias label: elementLabel.text

    // Like width+height, x+y, but in meters.
    property point position
    property size dimensions

    width: parent.width * (dimensions.width / parent.dimensions.width)
    height: parent.height * (dimensions.height / parent.dimensions.height)
    onWidthChanged:  x = position.x * (parent.width / parent.dimensions.width)
    onHeightChanged:  y = position.y * (parent.height / parent.dimensions.height)

    // Recalculate positions in meters
    onXChanged: { position.x = x / (parent.width / parent.dimensions.width) }
    onYChanged: { position.y = y / (parent.height / parent.dimensions.height) }

    transformOrigin: Item.TopLeft

    // Just some default value to stand out.
    color: "yellow"

    Settings {
        category: main.objectName
        property alias xPosition:   main.position.x
        property alias yPosition:   main.position.y
        property alias rotation:    main.rotation
    }

    Behavior on rotation {
        SpringAnimation { spring: 2; damping: 0.2; duration: 300 }
    }

    MouseArea {
        id: dragMouseArea
        acceptedButtons: Qt.AllButtons
        anchors.fill: parent
        drag.target: parent
        drag.minimumX: main.rotation ? main.height : 0
        drag.minimumY: 0
        drag.maximumX: main.parent.width  - (main.rotation ? 0 : main.width)
        drag.maximumY: main.parent.height - (main.rotation ? main.width : main.height )

        onClicked: {
            if (mouse.button === Qt.RightButton) {
                main.rotation = main.rotation ? 0 : 90;
            }
        }
    }
    Drag.active: dragMouseArea.drag.active

    Label {
        id: elementLabel
        rotation: - parent.rotation
        anchors.centerIn: parent
    }
}
