import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Room plan")

    Settings {
        category: "ApplicationWindow"
        property alias width: window.width
        property alias height: window.height
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        RoomPage {
            Room {
                dimensions: Qt.size(5.26, 4.15)

                // TODO: Obstacles. Should be a different type? Things like
                // settings, rotation or label don't apply.
                Element {
                    objectName: "livingRoomObstacle"
                    color: "black"
                    dimensions: Qt.size(0.44, 0.28)
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                }

                Element {
                    objectName: "livingRoomCouch"
                    label: qsTr("Sofá")
                    color: "brown"
                    dimensions: Qt.size(1, 2.7)
                }

                Element {
                    objectName: "livingRoomTable"
                    label: qsTr("Mesa")
                    color: "darkorange"
                    dimensions: Qt.size(0.96, 1.56)
                }

                Element {
                    objectName: "livingRoomFurniture"
                    label: qsTr("Mueble")
                    color: "lightsalmon"
                    dimensions: Qt.size(3.15, 0.52)
                }
            }
        }

        RoomPage {
            Room {
                dimensions: Qt.size(4, 2)
                Element {
                    objectName: "mainBedRoomFurniture"
                    label: qsTr("Armario")
                    dimensions: Qt.size(1, 0.2)
                }

                Element {
                    objectName: "mainBedRoomBed"
                    label: qsTr("Cama")
                    color: "grey"
                    dimensions: Qt.size(2, 1.60)
                }
            }
        }

        Page1 {
            clip: true
        }

    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton { text: qsTr("Comedor") }
        TabButton { text: qsTr("Dormitorio") }
        TabButton { text: qsTr("Cocina") }
    }
}
