import QtQuick 2.0

Rectangle {
    property size dimensions
    color: "lightgrey"

    anchors.centerIn: parent
    width: {
        if ((parent.width / parent.height) < aspectRatio())
            return parent.width;
        else
            return parent.height * aspectRatio();
    }
    height: width / aspectRatio()

    function aspectRatio() { return dimensions.width / dimensions.height; }
}
